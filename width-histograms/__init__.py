from re import split

import pint
ureg = pint.UnitRegistry()
import matplotlib.pyplot as plt
import matplotlib
ureg.setup_matplotlib(True)
import pandas as pd

import numpy as np

from scipy.stats import norm
from scipy.optimize import curve_fit

def getDf(fname):
    df = pd.read_csv(fname)
    df['frequencies'] = (ureg.speed_of_light/(df['wavelength'].values*ureg.nm)).to('MHz').m
    df.attrs['fname'] = fname
    # We save in this attribute the modulation amplitude according to the file name
    df.attrs['mod_amp'] = int(split(r'[/-]', fname)[1])
    # best fit of data
    (mu, sigma) = norm.fit(df['frequencies'].values)
    df.attrs['sigma'] = sigma
    df.attrs['mu'] = mu
    return df

def plotHistogram(df, show=True):
    # the histogram of the data
    n, bins, patches = plt.hist(df['frequencies'].values, bins=100, label="Measurements Histogram")
    centers = (0.5*(bins[1:]+bins[:-1]))
    def gaussian(x,amp,mu,sig):
        return amp*np.exp(-(x - mu)**2/(2*sig**2))
    popt_guess = [max(n), df.attrs['mu'], df.attrs['sigma']]
    popt, pcov = curve_fit(gaussian, centers, n, p0=popt_guess)
    df.attrs['sigma_err'] = np.sqrt(pcov[2][2])
    fit_centers = np.linspace(min(centers), max(centers), 1000)
    plt.plot(fit_centers, gaussian(fit_centers, *popt), label='Gaussian Fit')
    plt.xlabel('Frequency [MHz]')
    plt.ylabel('Appearances [A.U]')
    plt.title("Width: ${:.3f}\pm{:.3f} [MHz]$".format(df.attrs['sigma'], df.attrs['sigma_err']))
    plt.legend()
    plt.savefig(df.attrs['fname'] + ".pdf")
    if show:
        plt.show()
    plt.clf()

dfs = [
    getDf("./50-int_histogram.csv"),
    getDf("./100-int_histogram.csv"),
    getDf("./200-int_histogram.csv"),
    getDf("./300-int_histogram.csv"),
    getDf("./350-int_histogram.csv"),
]

plotHistogram(dfs[0], show=False)
plotHistogram(dfs[1], show=False)
plotHistogram(dfs[2], show=False)
plotHistogram(dfs[3], show=False)
plotHistogram(dfs[4], show=False)

sigmas = [df.attrs['sigma'] for df in dfs]
sigmas_errs = [df.attrs['sigma_err'] for df in dfs]
mod_amps = [df.attrs['mod_amp'] for df in dfs]

plt.errorbar(mod_amps, sigmas, yerr=sigmas_errs, marker='o', linestyle='', mfc='none')
plt.xlabel("Modulation amplitude [A.U]")
plt.ylabel("Spectral Widths [MHz]")
#plt.legend()
plt.savefig('all-widths.pdf')
plt.savefig('all-widths.png')
plt.savefig('all-widths.pgf')
plt.show()
