13/03/2022
==========

First day in the lab - the 780 DFB LASER fell from the table when I moved it
out of Kruger's lab. It's connection legs got a small bend due to the fall.
Yuval thinks it don't think it caused a severe damage - the beam looks like
before, but only time will tell.

The first thing I set up to do was setting up the optical isolator in the right
angle - by rotating it and getting a maximum transmission - (48%-55% according
to the manual). Here are the measurements, done at:

- Temperature: 22.2 Celsius
- Laser diode current: 40.49 mA
- Noise amplitude: 0.0369 mW

Without isolation, amplitude: 6.57 mW With isolation, amplitude: 3.21 mW =
48.7%, angle: 330 degrees.

Here's a measurement of the isolation: I inverted the direction of the isolator
180 degrees so that the LASER was coming towards the output of the isolator,
and I got an amplitude of 0.074 mW - which means it decreases the signal by
99.44%. I think this corresponds to 39.951 dB I used the equation:

```
20*log10((1-(0.074-0.0369)/6.57)*100)
```

11/04/2022
==========

Today Yuval begged me to write content in the lab's shared one-note notebook.
This and the following paragraph is written at ~12:00 - before launch. Many
things happened today:

The near future's mission is to improve the quality of the signal - currently,
the signal is noisy, and the Doppler free peaks are not observable at all. The
noise seem to be characterized around ~13-15 KHz, and is very uniform, hardly
hidden using the scope's averaging. There are measurements from last week at
../rubidium-spectrum/saturated-absorbtion-04.04.2022\*.csv, which might testify
a different noise, at different characteristic frequency.

I managed to remove the background light noise of the lab's lighting with an
iris and a "pipe" of 1inch diameter placed before the photodiode. This hasn't
improved much the situation. I also uplifted the scope and the function
generator up to where all instruments are located, but it hasn't seem to helped
much.

The next mission is to evaluate and compare the noise from last week vs the
noise now.

A background mission is to install drivers and whatever needed to get data out
of the scope programmatically, and not via the web interface of the scope at
192.168.1.101 .

Currently, installation of the instrument at the windows computer connected to
the network the scope is connected, failed with error:

> Failed to install command set 'InfiniiVision 1200 X-Series and EDUX1052AG
> Oscilloscopes'. Could not load file or assembly 'Ivi.Visa.Interop,
> Version=3.0.0.0, Culture=neutral, PublickKeyToken=....' or one of it's
> dependencies. The system cannot find the file specified.

During one of the installations, I got an error that didn't halt it, with a
message mentioning the words `Ivi.Visa.Interop`, can't remember which
installation was it.

25/04/2022
==========

Today is the 2nd day after passover, Yuval wasn't here today, he was in
Weizmann's Institute.

The noise from the acquisitions was diagnosed and indeed it's in the regime of
~18-19 KHz, as can be seen in the images at: 

../rubidium-spectrum/spectrum of modulation signal 04.04 and 11.04.png

Additionally, I managed to install many Python and other packages on the
Windows machine, and fix the connection errors to the oscilloscope. I also
managed to run an example Python script from Keysight's website, now saved in
the repository (not yet git pushed) on the machine. The script seemed to alter
the settings of the scope in some way, and I didn't managed to figure out how
does it spit the data.

Additionally, I installed many convenience packages to the Windows machine (not
via Nix, unfortunately). List is:

- Git
- Poetry
- Python
- NodeJS + NPM + Chocolate
- Direnv
- Neovim

Some python packages for Neovim and Nodejs were installed along the way. My
Neovim configuration from https://github.com/doronbehar/.config_nvim using was
installed as well, almost flawlessly, though the Windows symlinks system is
rather horrible, but I got through that.

Future mission is to figure out how to write a better version of the keysight
python script, and how to control the scope's settings the way I need them to
be. It'd also like to understand how the python packages pyvisa and `comptypes`
compare, and why keysight chose to use comtypes and not pyvisa.

Perhaps a more important misssion, would be to figure out how to clean the
signal. I'll need Yuval's help for this.

1/5/2022
========

## Status Estimation meeting with Yuval

Modulation signal for photo diode driver:

We start with a few 5-6 triangle signal periods, with which I'll find what is
the offset in which I should send a DC. 

1. $Triangle$ + $A \cos(2 \pi f t)$, with $f >> f_{triangle}$
2. Locking: offset chosen by finding the correct pick of the triangle in step 1 is used,
plus an error signal of $I \int_0^t e(t)\d t$ + $B \cos(2\pi f t)$ . The error signal e(t) is:

e(t) = \int_0^{1/\omega} B diode_signal(t) \cdot \cod(2 \pi f t + \phi)\d t

Parameters are:

$\phi$, $I$, $A$ and $B$

9/5/2022
========

(writing this at 30/5/2022): This day was the day Yael came, and I worked only
until ~18:00. I played in this day with the Moku go and I realised it's not
suitable for our purposes - it's not easily programmable, it has a lock-in
amplifier but it's impossible to design it's behavior to our purposes.

16/5/2022
=========

(writing this at 30/5/2022): I tried to communicate with the vacuum controllers
via Labview. They appeared to perform some tasks as expected, but communication
seemed like it got broken after some queries were made. We sent an email to
Pfeiffer about the situation.

23/5/2022
=========

(writing this at 30/5/2022): I installed the OS of the Red Pitaya, and
installed the PID lock-in web application by Marceluda, also helped Itay
installing it and playing with the Linux of it, and made order in the Lab.

24/5/2022
=========

(writing this at 30/5/2022): Group meeting. We all talked about all the project
we need and who is working on what sort of.. Yuval wrote something in his
one-note notebook.

30/5/2022
=========

I managed to get a very nice signal out of the optical system into the
red-pitaya! The optical rays are a bit off though - blocking the probe and
leaving the pump as is doesn't give an absorption signal as expected. OTH, the
signal as a whole looks pretty good. It seems that some of the signal's power
comes from the probe as well.

The signal still can be improved. What's most bothering is the fact that
there's a constant drift as if DC is infiltrating the modulation signal. For
example, now, at the time of writing the LD current is set to ~103mA and the
HEX digits on the display present ~115mA oscillating at ~+/-0.02mA . This means
that there's DC current of around 12mA !!! Don't know how to explain this..

Additional notes:

- I used the Jumper on both analog fast inputs to set them to HV setting of
  +/-20V. This seems to have helped the signal to have a broader range in the
  scope.
- I noticed the configs of the original web app don't load when used in the
  "harmonic" version of the web app. They make the web server stuck to the
  point where a reboot is required.
- An screencast from this date is saved on mu computer

To summarise, I realised today how to "find" the desired error signal, and what
are the parameters that I should play with in order to achieve it, but I didn't
manage to find them for my setup... The optical parameters can be improved,
which is something I avoided playing with.

06/06/2022
==========

No physical progress today unfortunately. Came at ~10:00. Started to regenerate
the signals from last time. The DC offset I saw last time was in the orders of
~2mA today, I don't know why. Motivated to understand better the meaning of the
AUX registers because the signals I managed to generate were even worse then
before, I started to dive into the FPGA, c, and HTML code of the project. Yuval
seemed busy so it felt like the right decision as to how to spend the time of
the day. This goal made me almost setup a development environment for the
red-pitaya projects by Marceluda. Hence I created the `nix-xilinx` repository
(based on `nix-matlab`). As of writing it's available only locally. Installing
Xilinx' tools, requires downloading ~50GB of software, which is something I
don't want to do on the slow Technion WiFi. I also hope that I figured out
correctly what gcc cross compiler to use for these compilations - when vivado
will be installed as well, we'll know - this change required changes to the
`settings.sh` file in Marceluda's repos.
