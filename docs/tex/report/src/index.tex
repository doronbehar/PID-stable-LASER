\maketitle

\abstract{Our lab's goal is performing precision spectroscopy of Chiral
	Molecular ions. To this end I stabilized a reference LASER using a lock-in
	amplifier, and a typical Doppler-free spectroscopy optical setup. I locked
	the LASER to a certain spectral line of a hyperfine quantum state
	transition in Rubidium. The locked LASER's spectral width was measured to
be as narrow as $\sim 0.3 MHz$, narrow enough for calibrating our wavelength
meter.}

\begin{multicols}{2}

\section{Introduction}

Our main experiment's goal is to perform precision vibrational spectroscopy of
trapped chiral molecular ions and search for a difference between left and
right handed molecules. Such a measurement requires cooling the molecular ions
and vibrationally exciting them with a LASER. We cool the molecular ions with
cotrapped atomic ions to lower Doppler broadenings - a process called
sympathetic cooling. Hence our LASER's wavelengths must be accurately
stabilized.

Being stable requires a stable "meter ruler" for light's wavelengths. Such an
instrument is called a wavelength meter. Every wavelength meter can be tuned to
change "the spacing" between the ruler's graduations. This raises the
challenge: How do you define "meter" stably in your lab?

Since the speed of light is constant, defining a meter, a Hz or a second is
equivalent. The SI system of units defined in 1967 the frequency $\Delta \nu_{Cs}$ - a
certain hyperfine transition of Caesium 133 as constant, and thus defined the
Hz quantity. Similarly to $\Delta \nu_{Cs}$, I used the hyperfine
transition $5^2 S_{1/2} F=3 \rightarrow 5^2 P_{3/2} F=2$ of \ce{^{87}Rb} to
absolutely define a certain frequency for our lab's wavelength
meter\footnote{Product WS7-60 by HighFinesse}.

\subsection{Terms Definitions}

The wavelength meter's \textit{deviation sensitivity} is of 1 MHz. This means
that at any time, the wavelength meter can differentiate between two light
sources when their frequency difference is as low as 2 MHz. However, over long
periods of time such as a few minutes (probable duration for an experiment), a
truly static frequency can be measured with up to 60 MHz deviation. This
defines the \textit{absolute accuracy} of the wavelength meter. After weeks and
months, the deviation can reach up to hundreds of MHz - requiring the user to
\textit{calibrate} their wavelength meter from time to time.

Lastly, a LASER controlled via a feedback loop and lasing at a specific
wavelength of a specific spectral line, is considered \textit{locked} to this
spectral line.

\subsection{Project's Requirements}

One of the Lab's mission is LASER cooling (using CW LASERs). Hence, to reach
minimal temperatures, we \textit{calibrate} our wavelength meter daily. A
diagram of the project's part in the lab is presented in figure
\ref{fig:project-goal}.

Absolutely calibrating the wavelength meter is done using a LASER locked to a
certain known spectral line of \ce{^{87}Rb}. Since the deviation sensitivity of
our wavelength meter is limited to 1 MHz, the spectral width I strived to reach
when locking my LASER is of the orders of 1 MHz as well. Eventually I reached
spectral widths as low as 0.3 MHz.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{../../images/project-goal.pdf}
	\caption{Project Goal \& Scope}
	\label{fig:project-goal}
\end{figure}

\section{Controlling a LASER}

The LASER I used for the calibration is a DFB
Laser\footnote{\texttt{EYP-DFB-0780-00040-1500-BFW11-0005} by Eagleyard}
(figure \ref{fig:laser-diode}), and it's wavelength depends on it's drive
current and diode temperature. To control these parameters I used a LASER diode
controller\footnote{\texttt{LDC501m} by SRS} (figure
\ref{fig:laser-diode-controller}) that continuously sets these parameters with
feedback by connecting directly to the LASER diode.

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=\linewidth]{./LASER-diode.png}
	\end{center}
	\caption{The DFB LASER diode}
	\label{fig:laser-diode}
\end{figure}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=\linewidth]{./LASER-diode-controller.png}
	\end{center}
	\caption{The LASER diode controller}
	\label{fig:laser-diode-controller}
\end{figure}

The LASER diode is connected to the controller via cables I built myself. I
mounted it on a plate suitable for assembling on an optics table, along with
another plate holding the cables' plugs. I designed the plates and built a CAD
model for them with AutoCAD.\footnote{Thanks to help from Anatoly Bekkerman
from the chemistry faculty.} An assembly of the design is illustrated in
figures \ref{fig:laser-diode-CAD} and imaged in figure
\ref{fig:laser-diode-connected}.

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=\linewidth]{./LASER-diode-assembly.png}
	\end{center}
	\caption{The LASER diode plates CAD design}
	\label{fig:laser-diode-CAD}
\end{figure}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=\linewidth]{./LASER-diode-connected.png}
	\end{center}
	\caption{The LASER diode connected to the plates}
	\label{fig:laser-diode-connected}
\end{figure}

\section{LASER Spectroscopy Methods}

\ce{^{87}Rb} and \ce{^{85}Rb} are vapors in low pressures, making them suitable to
act as absorption mediums at room temperature. The \ce{Rb} medium I used came
as a vapor in a Quartz cell (figure \ref{fig:Rb-cell})\footnote{A Thorlabs
product.}. Pointing the LASER through the \ce{Rb} cell and straight into a
Photodiode, allows one to measure the absorption spectrum of \ce{Rb}.

\begin{figure}[H]
\begin{center}
	\adjincludegraphics[
	width=\linewidth,
	trim={0 {0.25\height} 0 {0.27\height}},
	clip
	]{./Rb-cell.jpg}
\end{center}
\caption{Rubidium vapor cell}
\label{fig:Rb-cell}
\end{figure}

Taking this naive setup 1 step further, one can see the absorption spectrum on
an oscilloscope, using a triangle wave to increase and decrease periodically
the wavelength of the LASER diode. An absorption spectrum of such a setup is
shown in the red signal in graph \ref{graph:absorption-measurement}. Such
periodic increase and decrease of the of drive current is essentially a
triangle signal, called a \textit{ramp}. This ramp appears in the blue line in
graph \ref{graph:absorption-measurement}.

Using the slope and amplitude of the ramp, one can deduce the spectral width of
the absorptions - the width of each of these "valleys" in the red absorption
signal, is $350 MHz$ ! This due the Doppler effect - some \ce{Rb} atoms see
the incoming LASER's photons as having a different frequency in their frame of
reference. Indeed, a theoretical calculation of Doppler effect around
$\lambda_0 = 780 nm$, using $ \expval{\frac{1}{2}m v^2} = \frac{3}{2} K_B T$
and Rubidium's yields:

\begin{equation}
\Delta f = \frac{\expval{v}}{c} \cdot f_0 \approx 380 [MHz]
\label{eq:Delta-f}
\end{equation}

This too wide spectral width makes it difficult to lock the LASER's wavelength
accurately enough. The solution is using two LASER beams coming from opposite
directions, creating a setup called "Saturated absorption".

\subsection{Saturated Absorption Spectroscopy}

Besides absorption, another interaction of matter with light is stimulated
emission - illustrated in figure \ref{fig:stimulated-emission}. An excited atom
absorbing a photon with the same frequency of the excitation, will emit a
photon at the same frequency, resulting in two photons emitted altogether.

\begin{figure}[H]
\begin{center}
	\includegraphics[width=\linewidth]{./stimulated-emission.pdf}
\end{center}
\caption{Stimulated Emission illustration (Creative Commons License)}
\label{fig:stimulated-emission}
\end{figure}

Consider a setup where two LASER beams are counter propagating as shown in
figure \ref{fig:two-laser-beams}. We'll consider the beams parallel enough,
although in my optical setup they have a relative angle of $\sim 5^{\circ}$.
Excited atoms in the vapor cell moving in a random direction will likely see a
different frequency for the "pump" beam vs the "probe" beam. If an atom is
moving in a tangent direction to both beams, the atom will see the same
frequency for photons from both beams, and we expect it to be stimulated to
emit a photon, causing a small peak in the absorption signal. See figure
\ref{fig:doppler-photon-absorption} for an illustration of overcoming Doppler
effect, and the orange line in graph \ref{graph:absorption-measurement} for a
measured spectrum with the two beams setup.

\begin{figure}[H]
\begin{center}
	\includegraphics[width=\linewidth]{./two-LASER-beams.pdf}
\end{center}

\caption{Two LASER beams of the same source hit \ce{Rb} vapor cell. The "Probe"
beam reaching the photodiode, has a lower power compared to the "Pump" beam.
"BS" is a Beam Splitter.}

\label{fig:two-laser-beams}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=\linewidth]{../../images/doppler-photon-absorption.png}

\caption{Source: \textit{"Spectroscopy in a new light"} Arthur L.
Schawlow (1982)}

\label{fig:doppler-photon-absorption}
\end{figure}

\begin{graph}[H]
\begin{center}
% TODO: Try to use the pgf
\includegraphics[width=\linewidth]{./absorption-measurement.pdf}
\end{center}

\caption{Measured absorption spectrum of Rubidium. Black: Doppler effect
broadening the spectrum; Orange: stimulated emissions finely marking
spectral lines; Dashed vertical lines: Several spectral lines of
\ce{^{87}Rb} from the $5^2 S_{1/2}$ states, to $5^2 P_{3/2} F=2$, aligning with
the peaks in the orange signal.}

\label{graph:absorption-measurement}
\end{graph}

Again, using the ramp's slope and amplitude we can deduce the spectral width of
the stimulated emission peaks. In the measurement above, the peaks where the
vertical spectral lines are marked, have a spectral width of $\sim 40 MHz$,
making them sufficiently narrow to lock the LASER to one of them.

\section{Locking The LASER}

Seeing both Rubidium's isotopes' spectrums in our oscilloscope is nice, but
remember we wish to lock our LASER to one of these spectral lines, and
calibrate our wavelength meter. The literature includes many references to
previous measurements of Rubidium's hyperfine transitions, scattered across
many articles. They are nicely collected in Dan Steck's "alkalidata D Line
Data" webpage\footnote{\url{https://steck.us/alkalidata/}} and it helped me
choose the spectral line to lock the LASER into.

\subsection{Choosing a Spectral Line}

Choosing a spectral line requires matching a stimulated emission peak to a
spectral line. The DFB LASER's manufacturer claims the LASER's wavelength is
linear to it's drive current. Using the not-yet-calibrated wavelength meter, I
measured the wavelength around which I searched for spectral lines. Using Dan
Steck's data, I created a graph presenting all spectral lines of Rubidium on a
wavelength axis. Comparing the peaks' distances from each other in my
absorption, to the graph listing the spectral lines, I identified the
\ce{^{87}Rb}$5^2 S_{1/2} \rightarrow 5^2 P_{3/2} F=2$ lines in the absorption,
as presented in graph \ref{graph:absorption-measurement}.

I created graph \ref{graph:absorption-measurement} by calculating the spacing
between the peaks in terms of voltage - via the ramp's linearity, and demanding
that the $F=3 \rightarrow F=2$ transition will be located at the correct peak.
This defines the affine transformation of the ramp's voltage to wavelength -
the peaks spacing defines the slope, and the position defines the offset.

Apparently, marking \textit{all} spectral lines on the absorption with the same
ramp voltage to wavelength transformation, doesn't align accurately the other peaks
to other spectral lines. This is probably due to the LASER's wavelength not
truly linear to the drive current, or perhaps linear only in narrow regions.
Still I had sufficient assurance in the identification of the red labeled
spectral line, also because it's distant enough from it's neighbors, and I
chose to lock the LASER to it.

Locking the LASER was done via a feedback loop and a lock-in amplifier, with
the photodiode's absorption signal as input, and the LASER diode controller's
Modulation as output.

\subsection{Lock-in amplifiers}

Assume you have a system that responds very finely to a change of a certain
parameter, and you wish to accurately lock that parameter to the value in which
the response is maximal. A lock-in amplifier is an amplifier capable of
producing a signal proportional to the derivative of the system's response near
the maximal response. Based on this derivative, one can \textit{lock} that parameter to
the value in which the response is maximal. In our case, the LASER's wavelength
controlled by the drive current is the parameter and the response is the
absorption signal from the photodiode.

The system implemented for locking the LASER is depicted in figure
\ref{fig:block-diagram}. To describe mathematically the amplifier's action,
we'll first explain what signal we get in $V_2(t)$, when locking isn't enabled
- when the "Lock! switch" is disconnected.

\begin{figure}[H]
\centering
\includegraphics[width=\linewidth]{./electrical-block-diagram.pdf}

\caption{Lock-in amplifier block diagram: $V_{LDC}$ is the signal changing the
LASER's wavelength around our target spectral line; $V_a(\lambda)$ is the
absorption signal; "Ramp" is the triangle signal with which you can see the
spectrum prior to locking; $V_1(t)$ and $V_2(t)$ are inner intermediate
signals.}

\label{fig:block-diagram}
\end{figure}

% TODO: Yuval: "many of these variables need definition. Think if you need so many"
\subsubsection{Generating a locking signal}

When we don't vibrate the system (when $V_s = 0$), we can define the LASER's wavelength:

\begin{equation}
\lambda_r(t) = \lambda_0 + v_r t
\label{equ:lambda_r}
\end{equation}

Where $\lambda_0$ is our target spectral
line's wavelength and $v_r$ is speed of change. Adding the vibration we get:

\begin{equation}
\lambda(t) = \lambda_r(t) + \lambda_s \sin(\omega t + \varphi)
\label{eq:lambda}
\end{equation}

Where $\lambda_s \propto V_s$ and $\lambda_s \ll \lambda_r$. The absorption
signal $V_a(\lambda(t))$ near $\lambda_0$ satisfies:

\begin{equation}
V_a(\lambda) \underset{\lambda_s \ll \lambda_r(t)}{\approx} V_a(\lambda_r(t)) + \eval{\pdv{V_a}{\lambda}}_{\lambda_r(t)} A_s \sin(\omega t + \varphi)
\label{eq:V_a-taylor}
\end{equation}

Where $A_s \propto V_s$ and $\omega$ is the vibration frequency. Multiplying
the vibration signal and $V_a(\lambda(t))$ gives:

\begin{equation}
V_1(t) = \sin(\omega t) \left(V_a(\lambda_r(t)) +
\eval{\pdv{V_a}{\lambda}}_{\lambda_r(t)}\sin(\omega t +
\varphi) A_s \right)
\label{eq:V_1}
\end{equation}

Writing the sines' product as a sum of cosines, we get a DC term proportional
to $\cos(\varphi)$. Only this term is left after the low pass filter, producing:

\begin{equation}
\boxed{V_2(t) \propto \eval{\pdv{V_a}{\lambda}}_{\lambda_r(t)}}
\label{eq:derivative}
\end{equation}

Graph \ref{graph:satspec} illustrates numerically the generation of the
derivative for a system responding maximally at a certain wavelength. 

% TODO: Yuval: "What are you trying to write?"
The derivative is added to the laser diode signal to correct the laser
wavelength and make it closer to the center of the spectral line from
either side of it.

This is the heart of the idea of locking.

\begin{graph}[H]
\centering
\includegraphics[width=\linewidth]{./satspec.pdf}

\caption{Numerical simulation for the derivative signal generation with a
Gaussian absorption signal of $0.2 sec$ width.}

\label{graph:satspec}
\end{graph}

The project's system was programmed to connect the "Lock! switch" when the ramp
signal reaches a point close enough to the maximal response - near the
stimulated emission peak.

\section{Locked LASER Spectral Width Assessment}

Locking the LASER includes finding an optimal set of many parameters. Some of
these parameters include the vibration amplitude $V_s$, and the phase $\varphi$
diagrammed in figure \ref{fig:block-diagram}. The optimal phase along with the
vibration frequency $\omega$ should fit the time it takes the system to respond
to the modulation, so that the DC term proportional to $\cos(\varphi)$ falling
after the low pass filter is maximal. I scanned by hand the phase parameter and
found the optimal phase. Finding the optimal vibration amplitude however, was a
bit more interesting:

Increasing $V_s$ means making the system more sensitive to the vibrations,
because $V_s$ is proportional to the DC term left in equation
\ref{eq:derivative}. However, since the vibration $V_s$ is active even when the
LASER is locked, it broadens the spectral width of the locked LASER.
Additionally, increasing $V_s$ comes at the price of dissatisfying the
approximation done in equation \ref{eq:V_a-taylor} where we assumed $V_s
\propto \lambda_s \ll \lambda_r(t) \sim \lambda_0$.

To assess the spectral width of the locked LASER, with different values of
$V_s$, I read programmatically the frequency measured by our wavelength meter
for each of these values, and generated a histogram to each value of $V_s$. One
such histogram is presented in graph \ref{graph:spectral-width-histogram}.

Performing the same frequency measurements and calculation for 4 more vibration
amplitudes, I generated graph \ref{graph:spectral-width-optimization},
illustrating that the narrowest spectral width of $0.286 MHz$ was achieved when
tweaking the vibration amplitude; The spectral width is defined here as the
standard deviation of the Gaussian fit. Naturally I could have taken more
measurements, and optimize this parameter even further, but this already
satisfies the wavelength meter's calibration requirement.

\begin{graph}[H]
\centering
\includegraphics[width=\linewidth]{../../../../width-histograms/200-int_histogram.csv.pdf}

\caption{Spectral width calculation using frequency measurements' histogram.
Width is defined as the Gaussian distribution's standard deviation.}

\label{graph:spectral-width-histogram}
\end{graph}

\begin{graph}[H]
\centering
% TODO: Use the pgf?
\includegraphics[width=\linewidth]{./spectral-widths.pdf}

\caption{Spectral width $\Delta f$ vs vibration amplitude $V_s$.}

\label{graph:spectral-width-optimization}
\end{graph}

\section{Outlook}

We have successfully locked our DFB LASER to a spectral line of \ce{^{87}Rb},
producing a light source "stable as nature" to calibrate our wavelength meter.
The spectral width we achieved is satisfyingly better then our wavelength
meter's demand. Future plans include automation of the calibration itself when
the LASER is locked, automatic spectral width measurement after locking, and
integrating the LASER and the calibration command to our experiment's system.

\section{Acknowledgments}

The system upon which I programmed the lock-in amplifier is Redpitaya's STEMLab
device\footnote{\url{https://redpitaya.com/product/stemlab-125-14/}}, using
Marcel Luda's web application for
Redpitaya.\footnote{\url{https://marceluda.github.io/rp_lock-in_pid}} All code
used to generate this report, it's graphs and diagrams is located at the
project's GitLab
repository.\footnote{\url{https://gitlab.com/doronbehar/PID-stable-LASER}}

\end{multicols}
