= Rubidium saturated absorption spectroscopy for Ultra narrow bandwidth LASER

{image of system}

Wavelength of a diode LASER changes when it's current changes. Around \lambda = 780 nm, +/-0.1 nm is equivalent to +/-50 GHz ! (due to \lambda = c/f), and we strive to a LASER as accurate as +/-1MHz

We use the absorption spectrum of Rubidium to lock the current of the LASER to the right value:

{image of spectrum}
