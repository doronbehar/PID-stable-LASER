== Diode LASER PLATA

=== D connectors fooor diode LASER

Search table for D connectors, we need two:

https://www.digikey.com/short/jqw0ntrv

Faveorite type of D connector:

https://www.digikey.com/en/products/detail/norcomp-inc/172-009-212R911/955242

=== Links for optic stands to integrate with:

https://www.newport.com/p/M-SP-3-PK

Datasheet is here:

https://www.newport.com/mam/celum/celum_assets/np/resources/SP-xx_WEB_DWG.pdf

== To select

=== Photodiode

large area = longer response time

From: https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=285

file:///home/doron/repos/PID-stable-LASER/docs/FDS10X10-SpecSheet.pdf

Cermaic:

file:///home/doron/repos/PID-stable-LASER/docs/FDS100-SpecSheet.pdf
file:///home/doron/repos/PID-stable-LASER/docs/FDS100-SpecSheet.pdf

Balanced PD detector = nuetralizes amplitude noise via analog subtraction of original signal vs modified signal.

Do we need a balanced PD detector?

NO.



=== Connector from Photodiode to SMA (ADC of Artiq)

With a free space PD detector we don't need a cable.

Manufacture it by hand or buy one? Considering the photodiodes above...

=== Mirror

"Ultrafast" mirrors:

https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=6835

Humbler mirror:

https://www.thorlabs.com/thorproduct.cfm?partnumber=BB05-E03

Polaris Mount:

https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_ID=12931

Polaris Clamping arm:

https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_ID=8499

Polaris Posts:

https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_ID=9079

Buy 3-4 sets of mounts and posts

Why does a damage threshold is in Watt/cm and not Watt/(cm^2)

=== Lambda/4

https://www.thorlabs.com/thorproduct.cfm?partnumber=WPQ10M-780

Do I need posts etc? https://www.thorlabs.com/navigation.cfm?guide_id=2065

=== Collimator?

Do I need a Collimator for the rubidium cell?

https://www.thorlabs.com/thorproduct.cfm?partnumber=GC25075-RB

What is an Optical path length? Why is it relevant?

== Final list

Rb cell:

1 https://www.thorlabs.com/thorproduct.cfm?partnumber=GC25075-RB

Rb cell holder:

2 https://www.thorlabs.com/thorproduct.cfm?partnumber=VC3C/M

Batteries for PD:

10: https://www.thorlabs.com/thorproduct.cfm?partnumber=A23

Mirrors:

https://www.thorlabs.com/thorproduct.cfm?partnumber=BB05-E02-10

PD:

2 https://www.thorlabs.com/images/xlarge/TTN133878-xl.jpg

PBS:

2 https://www.thorlabs.com/thorproduct.cfm?partnumber=PBS052
1 https://www.thorlabs.com/thorproduct.cfm?partnumber=PBS202

Wave plates:

1 https://www.thorlabs.com/thorproduct.cfm?partnumber=WPQ05M-780
3 https://www.thorlabs.com/thorproduct.cfm?partnumber=WPH05M-780

Fiber:

1 https://www.thorlabs.com/thorproduct.cfm?partnumber=P5-780PM-FC-2

Collimator:

1 https://www.thorlabs.com/thorproduct.cfm?partnumber=CFC11A-B

Collimator holder:

Threaded:
1 https://www.thorlabs.com/thorproduct.cfm?partnumber=AD15F2
Not Threaded
1 https://www.thorlabs.com/thorproduct.cfm?partnumber=AD15NT

= Notes to myself

Why would I reverse the direction of the `PD` and `LD`?

It doesn't matter much.. The PD projects current according to amount of light and it's just used as an indicator.

The energy gap of "Rb I" we are looking for is at 780.0268nm:

https://physics.nist.gov/cgi-bin/ASD/lines1.pl?spectra=Rb&limits_type=0&low_w=&upp_w=&unit=1&submit=Retrieve+Data&de=0&format=0&line_out=0&en_unit=0&output=0&bibrefs=1&page_size=15&show_obs_wl=1&show_calc_wl=1&unc_out=1&order_out=0&max_low_enrg=&show_av=2&max_upp_enrg=&tsb_value=0&min_str=&A_out=0&intens_out=on&max_str=&allowed_out=1&forbid_out=1&min_accur=&min_intens=&conf_out=on&term_out=on&enrg_out=on&J_out=on

TEC+- and Thermometer+- are connected to the current controller- LDC500 from SRS in it's back. Connections are described in page 32 in the datasheet:

file:///home/doron/repos/PID-stable-LASER/docs/LDC501m.pdf
