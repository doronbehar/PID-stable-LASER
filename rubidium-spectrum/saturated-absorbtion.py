import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '.'))
from theoretical import lines, wlens
from theoretical import ureg
Q_ = ureg.Quantity

import pint
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['axes.formatter.useoffset'] = False
ureg.setup_matplotlib(True)
import pandas as pd
from uncertainties import ufloat, umath

import numpy as np
from scipy import fft
from scipy.signal import find_peaks

import re

def getDf(fname, center_wavelen_n):
    df = pd.read_csv(fname, skiprows=[1])
    df.attrs = {
        'samp_period': ufloat(
            df['x-axis'].diff().mean(),
            df.get('x-axis').diff().std()
        )*ureg.s,
        'fname': fname,
        # Center wave length according to wave meter when modulation is turned off:
        # Fits for ./saturated-absorbtion.csv
        'center_wavelen': ufloat(center_wavelen_n, 0.00001)*ureg.nm
    }
    if re.search(r'with-doppler', fname):
        df.attrs['doppler'] = True
    else:
        df.attrs['doppler'] = False
    df.attrs['peaks'], _ = find_peaks(df['1'].values, distance=30, width=10)
    # NOTE: Modulation is in channel 2, intensity is in channel 1
    df['f-axis'] = pd.Series(modulation2freq(df['2'].values*ureg.volt, df.attrs['center_wavelen']))
    df['lambda-axis'] = pd.Series(modulation2wavelength(df['2'].values*ureg.volt, df.attrs['center_wavelen']))
    df['affined'] = df['1'].values-6
    return df
# calibrated by comparing the voltage diff of the modulation, and dan steck's
# spectral lines 87Rb spectral lines from theoretical/__init__.py
MODULATION_RATIO = 0.11093216084773122 * ureg.nm / ureg.volt
def modulation2wavelength(modulation_voltage, center_wavelen_n):
    # lambda is a reserved word :)
    return center_wavelen_n + modulation_voltage*MODULATION_RATIO
def modulation2freq(modulation_voltage, center_wavelen_n):
    # lambda is a reserved word :)
    wavelen = modulation2wavelength(modulation_voltage, center_wavelen_n)
    return (ureg.speed_of_light/wavelen).to('THz')
def plotAbsorption(df, show=True, frequencyDomain=False, withModulation=True,clr=None):
    if frequencyDomain:
        x_vals = [val.n for val in df['f-axis'].values]*ureg.terahertz
        xlabel = 'Frequency [THz]'
    else:
        x_vals = [val.n for val in df['lambda-axis'].values]*ureg.nm
        xlabel = 'Wavelength [nm]'
    if df.attrs['doppler']:
        absorption_label = "Absorption (doppler broadened)"
    else:
        absorption_label = "Absorption"
    if withModulation:
        plt.plot(x_vals, df['2'].values*ureg.volt*30, '.', label="LASER current ramp", markersize=0.1)
    plt.plot(
        x_vals,
        # Emulate what the scope does when using the knobs to place the signal
        # at the center of the screen.
        df['affined'].values*ureg.volt,
        '.',
        label=absorption_label,
        color=clr,
        markersize=0.25
    )
    if len(df.attrs['peaks']):
        plt.plot(
            x_vals[df.attrs['peaks']],
            # Emulate what the scope does when using the knobs to place the signal
            # at the center of the screen.
            df['affined'].values[df.attrs['peaks']]*ureg.volt,
            'x',
            markersize=4
        )
    plt.xlabel(xlabel)
    plt.ylabel('Voltage [V]')
    plt.legend(markerscale=30, loc='lower left')
    if show:
        # Using a generic name - I rename the figures manually
        plt.savefig("current-plot.png")
        plt.savefig("current-plot.pdf")
        plt.savefig("current-plot.pgf")
        plt.show()

def plotNoise(df, show=True):
    frequencies = fft.fftfreq(
        len(df),
        df.attrs['samp_period'].magnitude.nominal_value
    )*0.001*ureg.kilohertz
    plt.plot(frequencies, np.abs(fft.fft(df['1'].values)))
    if show:
        plt.show()

# Fits 04.04.2022-{0..3}
#df0 = getDf("./saturated-absorbtion.csv", 780.02692)
#df1 = getDf("./saturated-absorbtion-04.04.2022-3.csv", 780.02723)
#df2 = getDf("./saturated-absorbtion-04.04.2022-4.csv", 780.03017)
#df3 = getDf("./saturated-absorbtion-11.04.2022-780.02988nm.csv", 780.02988)
#df4 = getDf("./saturated-absorbtion-13.08.2022.csv", 780.2460128)
#df5 = getDf("./saturated-absorbtion-13.08.2022-no-probe.csv", 780.2460128)
# Center wavelength here was computed by the code at the end of this file
df6 = getDf("./saturated-absorbtion-21.09.2022-no-doppler.csv",   780.2377849804406)
df7 = getDf("./saturated-absorbtion-21.09.2022-with-doppler.csv", 780.2377849804406)
# Partly copied from theoretical/__init__.py
relevant_lines = []
for l in lines:
    if l['isotope'] == 87 and l['F_f'] == 2 and l['P_f'] == "3/2":
        relevant_lines.append(l)
colors = plt.cm.rainbow(np.linspace(0, 1, len(relevant_lines)))
for l,clr in zip(relevant_lines, colors):
        if l['F_i'] == 3:
            label_F_i_is = "\\mathbf{{F={}}}".format(l['F_i'])
            label="${{}}^{{{isotope}}}$Rb $5^2 S_{{1/2}} {F_i_is} \\rightarrow 5^2 P_{{{P_f}}} F={F_f}$".format(
                isotope=l['isotope'],
                F_i_is=label_F_i_is,
                F_f=l['F_f'],
                P_f=l['P_f'],
            )
        else:
            label=None
        plt.axvline(
            l['wlen'].m.n,
            color=clr,
            linestyle='--',
            label=label
        )
plotAbsorption(df6, withModulation=False, show=False, clr='orange')
plotAbsorption(df7, withModulation=False, clr='black')

##### Calibration code for finding center wavelength and voltage to nm ratio
##### and center wavelength

cal_indices = df6.attrs['peaks'][[6,7]]
right_line_voltage = df6['2'].values[cal_indices[1]]
left_line_voltage = df6['2'].values[cal_indices[0]]
from pprint import PrettyPrinter
pp = PrettyPrinter(indent=2).pprint
for l in lines:
    if l['isotope'] == 87 and l['F_i'] == 3 and l['F_f'] == 2 and l['P_f'] == "3/2":
        left_line = l
    elif l['isotope'] == 87 and l['F_i'] == 2 and l['F_f'] == 2 and l['P_f'] == "3/2":
        right_line = l
nm_to_voltage_ratio = \
(right_line['wlen'] - left_line['wlen']).m.n/ \
(right_line_voltage - left_line_voltage)
print("nm_to_voltage: {}".format(nm_to_voltage_ratio))
right_center_wlen = right_line['wlen'].m.n - right_line_voltage*nm_to_voltage_ratio
left_center_wlen = left_line['wlen'].m.n  - left_line_voltage*nm_to_voltage_ratio
print("right_center_wlen: {}".format(right_center_wlen))
print("left_center_wlen:  {}".format(left_center_wlen))
right_center_wlen + right_line_voltage*nm_to_voltage_ratio
