import os

# based on Dan Steck's Rubidium data from his website

import pint
ureg = pint.UnitRegistry()
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['axes.formatter.useoffset'] = False
ureg.setup_matplotlib(True)
import pandas as pd
import numpy as np
from uncertainties import ufloat, umath

Rb87_P32_main = ufloat(384.2304844685, 0.00000000062)*ureg.THz
Rb85_P32_main = ufloat(384.230406373 , 0.0000000014 )*ureg.THz

Rb87_P32_F3 = ufloat(+197.7407, 0.0046)*ureg.MHz
Rb87_P32_F2 = ufloat(-072.9112, 0.0032)*ureg.MHz
Rb87_P32_F1 = ufloat(-229.8518, 0.0056)*ureg.MHz
Rb87_P32_F0 = ufloat(-302.0738, 0.0088)*ureg.MHz

Rb85_P32_F4 = ufloat(+100.205, 0.044)*ureg.MHz
Rb85_P32_F3 = ufloat(-020.435, 0.051)*ureg.MHz
Rb85_P32_F2 = ufloat(-083.835, 0.034)*ureg.MHz
Rb85_P32_F1 = ufloat(-113.208, 0.084)*ureg.MHz

Rb87_S12_F2 = ufloat(+2.563005979089109, 0.000000000000034)*ureg.GHz
Rb87_S12_F1 = ufloat(-4.271676631815181, 0.000000000000056)*ureg.GHz

Rb85_S12_F3 = ufloat(+1.2648885163, 0.0000000025)*ureg.GHz
Rb85_S12_F2 = ufloat(-1.7708439228, 0.0000000035)*ureg.GHz

Rb87_P12_main = ufloat(377.107463380, 0.000000011)*ureg.THz
Rb85_P12_main = ufloat(377.107385690, 0.000000046)*ureg.THz

Rb87_P12_F1 = ufloat(-509.06, 0.0079)*ureg.MHz
Rb87_P12_F2 = ufloat(+305.44, 0.0049)*ureg.MHz

Rb85_P12_F2 = ufloat(-210.923, 0.099)*ureg.MHz
Rb85_P12_F3 = ufloat(+150.659, 0.071)*ureg.MHz

# Order does matter, for greater choices of colors and line styles. `P_f: 3/2`
# stands for quantum state 5^2P_{3/2}
lines = [
    { 'freq': Rb85_P32_main + Rb85_P32_F1 - Rb85_S12_F2, 'isotope': 85, 'F_i': 1, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F0 - Rb87_S12_F1, 'isotope': 87, 'F_i': 0, 'F_f': 1, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F1 - Rb85_S12_F3, 'isotope': 85, 'F_i': 1, 'F_f': 3, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F0 - Rb87_S12_F2, 'isotope': 87, 'F_i': 0, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F2 - Rb85_S12_F2, 'isotope': 85, 'F_i': 2, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F1 - Rb87_S12_F1, 'isotope': 87, 'F_i': 1, 'F_f': 1, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F2 - Rb85_S12_F3, 'isotope': 85, 'F_i': 2, 'F_f': 3, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F1 - Rb87_S12_F2, 'isotope': 87, 'F_i': 1, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F3 - Rb85_S12_F2, 'isotope': 85, 'F_i': 3, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F2 - Rb87_S12_F1, 'isotope': 87, 'F_i': 2, 'F_f': 1, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F3 - Rb85_S12_F3, 'isotope': 85, 'F_i': 3, 'F_f': 3, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F2 - Rb87_S12_F2, 'isotope': 87, 'F_i': 2, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F4 - Rb85_S12_F2, 'isotope': 85, 'F_i': 4, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F3 - Rb87_S12_F1, 'isotope': 87, 'F_i': 3, 'F_f': 1, 'P_f': "3/2"},
    { 'freq': Rb85_P32_main + Rb85_P32_F4 - Rb85_S12_F3, 'isotope': 85, 'F_i': 4, 'F_f': 3, 'P_f': "3/2"},
    { 'freq': Rb87_P32_main + Rb87_P32_F3 - Rb87_S12_F2, 'isotope': 87, 'F_i': 3, 'F_f': 2, 'P_f': "3/2"},
    { 'freq': Rb85_P12_main + Rb85_P12_F2 - Rb85_S12_F2, 'isotope': 85, 'F_i': 2, 'F_f': 2, 'P_f': "1/2"},
    { 'freq': Rb85_P12_main + Rb85_P12_F2 - Rb85_S12_F3, 'isotope': 85, 'F_i': 2, 'F_f': 3, 'P_f': "1/2"},
    { 'freq': Rb85_P12_main + Rb85_P12_F3 - Rb85_S12_F2, 'isotope': 85, 'F_i': 2, 'F_f': 2, 'P_f': "1/2"},
    { 'freq': Rb85_P12_main + Rb85_P12_F3 - Rb85_S12_F3, 'isotope': 85, 'F_i': 2, 'F_f': 3, 'P_f': "1/2"},
    { 'freq': Rb87_P12_main + Rb87_P12_F1 - Rb87_S12_F1, 'isotope': 87, 'F_i': 1, 'F_f': 1, 'P_f': "1/2"},
    { 'freq': Rb87_P12_main + Rb87_P12_F1 - Rb87_S12_F2, 'isotope': 87, 'F_i': 1, 'F_f': 2, 'P_f': "1/2"},
    { 'freq': Rb87_P12_main + Rb87_P12_F2 - Rb87_S12_F1, 'isotope': 87, 'F_i': 2, 'F_f': 1, 'P_f': "1/2"},
    { 'freq': Rb87_P12_main + Rb87_P12_F2 - Rb87_S12_F2, 'isotope': 87, 'F_i': 2, 'F_f': 2, 'P_f': "1/2"},
]
freqs = list()
wlens = list()

for idx,l in enumerate(lines):
    if (l['P_f'] == "1/2" and ("SHOW_P12_RANGE" in os.environ)) or (l['P_f'] == "3/2" and ("SHOW_P12_RANGE" not in os.environ)):
        # For later use in xlim
        freqs.append(l['freq'])
        wlen = (ureg.speed_of_light/l['freq']).to('nm')
        wlens.append(wlen)
        lines[idx].update({'wlen': wlen})

toMHz = np.vectorize(lambda f: f.to('MHz'))
freqs_diffs = np.apply_along_axis(toMHz, 0, np.diff(np.sort(freqs)))

if __name__ == "__main__" and not ("NO_INTERACTIVE" in os.environ):
    colors = plt.cm.rainbow(np.linspace(0, 1, int(len(freqs)/2)))
    fig, axs = plt.subplots(2,1)
    for axs_idx,arr,xlabel in zip([0,1], [freqs, wlens], ['Frequency [THz]', 'Wavelength [nm]']):
        axs[axs_idx].set_xlim([
            np.mean(arr).m.n - 1.1*(np.mean(arr).m.n - np.min(arr).m.n),
            np.mean(arr).m.n - 1.1*(np.mean(arr).m.n - np.max(arr).m.n),
        ])
        # y_i is for putting "errorbars" in different heights - apparantly they are too short
        for l,f_or_wl,y_i in zip(lines, arr, np.linspace(1,1+len(arr),len(arr)+1)):
            label_dict = {
                'label': "${{}}^{{{isotope}}}$Rb $5^2S_{{1/2}} F={F_i} \\rightarrow 5^2 P_{{{P_f}}} F={F_f}$".format(
                    isotope=l['isotope'],
                    F_i=l['F_i'],
                    F_f=l['F_f'],
                    P_f=l['P_f'],
                    #  freq=l['freq'].to('MHz'),
                )
            }
            if l['isotope'] == 85:
                linestyle='-'
                if axs_idx == 0:
                    optional_label = {}
                else:
                    optional_label = label_dict
            else:
                linestyle='--'
                if axs_idx == 0:
                    optional_label = label_dict
                else:
                    optional_label = {}
            clr=colors[int(y_i/2 - 1)]
            axs[axs_idx].axvline(
                f_or_wl.m.n,
                color=clr,
                linestyle=linestyle,
                **optional_label
            )
            axs[axs_idx].axhline(
                y=y_i,
                xmin=f_or_wl.m.n-f_or_wl.m.s,
                xmax=f_or_wl.m.n+f_or_wl.m.s,
                color=clr
            )
        axs[axs_idx].set_xlabel(xlabel)
        axs[axs_idx].set_yticks([])
        axs[axs_idx].legend(loc='center', bbox_to_anchor=(0.4 + 0.2*axs_idx,0.5))

    cf = plt.gcf()
    plt.show()
    # Size to fit the article
    cf.set_size_inches((6, 6), forward=False)
    cf.savefig("theoretical.png")
    cf.savefig("theoretical.pgf")
    cf.savefig("theoretical.pdf")
