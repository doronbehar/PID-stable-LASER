import sys, os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '.'))
from theoretical import lines
from theoretical import ureg
Q_ = ureg.Quantity

from uncertainties import ufloat, umath

# current vs wavelength ratio written in the Diode's datasheet, computed by
# getting the distances in pixels from the datasheet linear graphs
# available in ../docs/EYP-DFB - LASER Diode - graphs.pdf
WAVELEN_CURRENT_RATIO = 0.1358598855*ureg.nm/(60*ureg.mA)
# 25 mA/volt modulation ratio is written upon the LASER diode controller
MODULATION_RATIO = 25 * ureg.mA / ureg.volt * WAVELEN_CURRENT_RATIO

# The line we target is by chance the last line: F=3 -> F=3, isotope 87
TARGET_CENTER_WAVELEN = (ureg.speed_of_light/lines[-1]['freq']).to('nm')

# These variables are for you to change
def calcFluctuations(
        miliseconds_fluctuations=3*ureg.ms,
        ramp_voltage_pp=62*ureg.mV,
        ramp_period=196.62*ureg.ms,
        unit='MHz' # could be set to 'femtometer' or any other length dimension
    ):
    # Use the wavelength fluctuation as std of the TARGET_CENTER_WAVELEN
    volt_per_sec = ramp_voltage_pp/(ramp_period/2)
    std = (miliseconds_fluctuations*volt_per_sec*MODULATION_RATIO).to('nm')
    wavelen = ufloat(TARGET_CENTER_WAVELEN.m.n, std.m)*ureg.nm
    requested_unit = Q_(1, unit)
    if requested_unit.check({'[time]' :-1}):
        return (ureg.speed_of_light/wavelen).to(unit)
    elif requested_unit.check('[length]'):
        return wavelen.to(unit)
