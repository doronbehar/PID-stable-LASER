
tau = 0.1;

f_dither = (1/tau)*50; 

T = 1/f_dither;

t = ((-18500*(T/(50))):(T/(50)):(18499*(T/(50))));
tau = 0.1;
figure(1); clf;
hold on;
%

a= 1;
f = @(t,t0) a*exp(-(t-t0).^2/(2*tau.^2));

modul = @(t) 0.01*tau*cos(2*pi*f_dither*t);
demod = f(t, modul(t)).*modul(t+2.5*T);

res = reshape(demod,50, length(t)/50);
tnew = t(1:50:end);

integ = sum(res,1);


plot( t, f(t,0))
plot( t, f(t, modul(t)));
plot( t, demod);
plot(tnew,1000*integ)
legend('Absorption', 'Absorption with FM', 'Demodulation', "Integral")